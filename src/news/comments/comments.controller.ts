import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { Comment } from './comments.interface';
import { CommentsService } from './comments.service';

@Controller('comments')
export class CommentsController {
  constructor(private readonly commentsService: CommentsService) {}
  @Get('/:newsId')
  @HttpCode(200)
  get(@Param('newsId') newsId: string | number) {
    return this.commentsService.find(newsId);
  }
  @Post('/:newsId')
  @HttpCode(201)
  create(@Param('newsId') newsId: string | number, @Body() comment: Comment) {
    return this.commentsService.create(newsId, comment);
  }
  @Delete('/:newsId/:commentId')
  @HttpCode(200)
  remove(
    @Param('newsId') newsId: string | number,
    @Param('commentId') commentId: string | number,
  ) {
    return this.commentsService.remove(newsId, commentId);
  }
  @Put('/:newsId/:commentId')
  @HttpCode(200)
  update(
    @Param('newsId') newsId: string | number,
    @Param('commentId') commentId: string | number,
    @Body() comment: Comment,
  ) {
    return this.commentsService.update(newsId, commentId, comment);
  }
}
