export interface Comment {
  id?: number;
  message: string;
  author: string;
}

export type Comments = Record<string | number, Comment[]>;
