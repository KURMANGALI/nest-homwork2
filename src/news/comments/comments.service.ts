import { Injectable } from '@nestjs/common';
import { getRandomInt } from '../news.service';
import { Comment, Comments } from './comments.interface';

@Injectable()
export class CommentsService {
  private readonly comments: Comments = {
    1: [
      {
        id: 1,
        message: 'Первое сообщения',
        author: 'Курмангали',
      },
    ],
  };

  find(newsId: string | number): Comment[] | string {
    if (this.comments[newsId]) {
      return this.comments[newsId];
    }
    return 'Коментарий не найдено!';
  }

  create(newsId: string | number, comment: Comment): Comment | string {
    const id = getRandomInt(0, 10000) as number;
    if (!this.comments[newsId]) {
      this.comments[newsId] = [];
    }
    this.comments[newsId].push({ id, ...comment });
    return 'Комментарий был создан!';
  }

  remove(
    newsId: string | number,
    commentId: string | number,
  ): null | Comment[] {
    if (!this.comments[newsId]) {
      return null;
    }
    const indexComment = this.comments[newsId].findIndex(
      (comment) => comment.id === +commentId,
    );
    if (indexComment === -1) {
      return null;
    }
    return this.comments[newsId].splice(indexComment, 1);
  }
  update(
    newsId: string | number,
    commentId: string | number,
    comment: Comment,
  ): null | any {
    if (!this.comments[newsId]) {
      return null;
    }
    const edetComment: Comment[] = this.comments[newsId].filter(
      (c) => c.id === +commentId,
    );
    let objComment: any = { ...edetComment };
    console.log(objComment[0]);
    objComment = { ...objComment[0], ...comment };
    console.log(objComment);
    return objComment;
  }
}
