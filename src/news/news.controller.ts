import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { renderNewsAll } from 'src/view/news/news-all';
import { renderTemlate } from 'src/view/template';
import { CommentsService } from './comments/comments.service';
import { CreateNewsDto } from './create.user.dto';
import { News, NewsEdit } from './news.interface';
import { NewsService } from './news.service';

@Controller('news')
export class NewsController {
  constructor(
    private readonly newsService: NewsService,
    // private readonly commentsService: CommentsService,
  ) {}
  @Get()
  @HttpCode(200)
  getNews() {
    return this.newsService.getAllNews();
  }

  @Get('/all')
  @HttpCode(200)
  getAllView() {
    const news = this.newsService.getAllNews();
    const content = renderNewsAll(news);

    return renderTemlate(content, {
      title: 'Список новостей',
      description: 'Самые интересные новости на свете!',
    });
  }

  @Get('/:id')
  @HttpCode(200)
  get(@Param('id') id: number) {
    return this.newsService.find(id);
    // const comments = this.commentsService.find(id);

    // return {
    //   ...news,
    //   comments,
    // };
  }
  @Post()
  @HttpCode(200)
  create(@Body() createNewsDto: News) {
    return this.newsService.create(createNewsDto);
  }

  @Delete('/:id')
  remove(@Param('id') id: number) {
    const isRemoved = this.newsService.remove(id);

    return isRemoved ? 'Новость удалена' : 'Передан неверный идентификатор';
  }
  @Patch('/:id')
  @HttpCode(200)
  edit(@Param('id') id: number, @Body() news: NewsEdit) {
    return this.newsService.edit(id, news);
  }
}
